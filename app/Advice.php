<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Image;
use Storage;

class Advice extends Model
{
	use SoftDeletes;

	protected $table = 'avisos';
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['titulo', 'subtitulo', 'descripcion', 'fecha', 'activo'];
	protected $dates = ['deleted_at'];

	/**
	 * Get all the advices.
	 *
	 * You could pass a query param to paginate. paginate = true
	 *
	 * @return Response
	 */
	public static function index()
	{
		$response = new Response();
		$paginate = request('paginate', false);
		try {
			if ($paginate) {
				$advices = Advice::paginate(10);
			} else {
				$advices = Advice::all();
				$return = $advices;
				if (request('mobile', false)){
					$advices = Advice::where('activo', 1)->orderBy('fecha', 'desc')->get();
					foreach ($advices as $a) {
						$a->imagen = url('storage/'.$a->imagen);
					}
					$groupedAdvices = $advices->groupBy('fecha')->toArray();
					$return = [];
					foreach ($groupedAdvices as $key => $value) {
						$temp = array('date' => $key, 'data' => $value);
						array_push($return, $temp);
					}
				}
			}
			$response->data = $return;
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->message = 'error';
			$response->code = 500;
		}
		return $response;
	}

	/**
	 * Get one advice by id.
	 *
	 * @param int $id Advice id
	 * @return Response
	 */
	public static function show($id)
	{
		$response = new Response();
		try {
			$advice =Advice::findOrFail($id);
			$advice->imagen = url('storage/'.$advice->imagen);
			$response->data = $advice;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->code = 500;
			$response->message = 'error';
		}
		return $response;
	}

	/**
	 * Create a new advice
	 * @param  Advice $advice Advice to create
	 * @return Response
	 */
	public static function store($request)
	{
		$response = new Response();
		try {
			$advice = new Advice();
			$image = Image::make($request['imagen']);
			$image->stream();
			$filename = time().'.jpg';
			$filepath = "avisos/{$filename}";
			Storage::put($filepath, $image);
			$request['imagen'] = $filepath;
			$advice->fill($request);
			$advice->imagen = $request['imagen'];
			$advice->save();
			$advice->imagen = url($advice->imagen);
			Notification::sendNotification($advice->titulo, ['advice' => $advice]);
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Updates the given Advice.
	 * @param  Advice $request Advice to edit
	 * @param  int $id      Advice ID
	 * @return Response
	 */
	public static function edit($request, $id)
	{
		$response = new Response();
		try {
			$advice = Advice::findorFail($id);
			$advice->fill($request);
			$advice->save();
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Soft deletes an instance of advice.
	 * @param  int $id Advice to delete
	 * @return Response
	 */
	public static function remove($id)
	{
		$response = new Response();
		try {
			$advice = Advice::destroy($id);
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

}
