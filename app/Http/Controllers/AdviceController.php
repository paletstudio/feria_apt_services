<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdviceController extends Controller
{
    public function index()
	{
		$response = \App\Advice::index();
		return response()->json($response)->setStatusCode($response->code);
	}

    public function show($id)
	{
		$response = \App\Advice::show($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function store(Request $request)
	{
		$advice = $request->all();
		$response = \App\Advice::store($advice);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function edit(Request $request, $id)
	{
		$advice = $request->all();
		$response = \App\Advice::edit($advice, $id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function remove($id)
	{
		$response = \App\Advice::remove($id);
		return response()->json($response)->setStatusCode($response->code);
	}
}
