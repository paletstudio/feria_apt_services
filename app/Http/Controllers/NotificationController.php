<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function sendNotification(Request $request) {
        $response = \App\Notification::sendNotification($request->input('message'));
        return response()->json($response)->setStatusCode($response->code);
    }
}
