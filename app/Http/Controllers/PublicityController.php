<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicityController extends Controller
{
	public function index()
	{
		$response = \App\Publicity::index();
		return response()->json($response)->setStatusCode($response->code);
	}

    public function show($id)
	{
		$response = \App\Publicity::show($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function store(Request $request)
	{
		$advice = $request->all();
		$response = \App\Publicity::store($advice);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function edit(Request $request, $id)
	{
		$advice = $request->all();
		$response = \App\Publicity::edit($advice, $id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function remove($id)
	{
		$response = \App\Publicity::remove($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function activePublicity()
	{
		$response = \App\Publicity::activePublicity();
		return response()->json($response)->setStatusCode($response->code);
	}
}
