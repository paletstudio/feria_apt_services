<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TarifasController extends Controller
{
    public function index()
	{
		$response = \App\Tarifas::index();
		return response()->json($response)->setStatusCode($response->code);
	}

    public function show($id)
	{
		$response = \App\Tarifas::show($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function store(Request $request)
	{
		$advice = $request->all();
		$response = \App\Tarifas::store($advice);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function edit(Request $request, $id)
	{
		$advice = $request->all();
		$response = \App\Tarifas::edit($advice, $id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function remove($id)
	{
		$response = \App\Tarifas::remove($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function activeRates()
	{
		$response = \App\Tarifas::activeRates();
		return response()->json($response)->setStatusCode($response->code);
	}
}
