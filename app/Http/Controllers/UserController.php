<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function login(Request $request)
    {
        $response = \App\User::login($request->only('email', 'password'));
        return response()->json($response)->setStatusCode($response->code);
    }

    public function create(Request $request)
    {
        $response = \App\User::create($request->all());
        return response()->json($response)->setStatusCode($response->code);
    }
    
}
