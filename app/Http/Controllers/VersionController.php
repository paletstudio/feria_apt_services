<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VersionController extends Controller
{
    public function get()
    {
        $response = \App\Version::get();
        return response()->json($response)->setStatusCode($response->code);
    }

    public function store(Request $request)
    {
        $response = \App\Version::store($request->all());
        return response()->json($response)->setStatusCode($response->code);
    }

    public function edit(Request $request)
    {
        $response = \App\Version::edit($request->all());
        return response()->json($response)->setStatusCode($response->code);
    }
}
