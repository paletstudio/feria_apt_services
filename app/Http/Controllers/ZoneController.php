<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZoneController extends Controller
{
	public function index()
	{
		$response = \App\Zone::index();
		return response()->json($response)->setStatusCode($response->code);
	}

    public function show($id)
	{
		$response = \App\Zone::show($id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function store(Request $request)
	{
		$advice = $request->all();
		$response = \App\Zone::store($advice);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function edit(Request $request, $id)
	{
		$advice = $request->all();
		$response = \App\Zone::edit($advice, $id);
		return response()->json($response)->setStatusCode($response->code);
	}

	public function remove($id)
	{
		$response = \App\Zone::remove($id);
		return response()->json($response)->setStatusCode($response->code);
	}
}
