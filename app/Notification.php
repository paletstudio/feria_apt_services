<?php

namespace App;

class Notification
{
    public static function sendNotification($msg = '', $data = [] )
    {
        $response = new Response();
        try {
            $content = array(
                "en" => $msg
            );
    
            $fields = array(
                'app_id' => config('onesignal.app_id'),
                'contents' => $content,
                'included_segments' => ['All'],
                'headings' => [
                    'en' => 'Tarjeta Feria'
                ],
            );

            if(count($data) > 0) {
                $fields['data'] = $data;
            }
    
            $fields = json_encode($fields);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic '.config('onesignal.api_key')));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    
            $result = curl_exec($ch);
            curl_close($ch);
            $response->message = 'success';
        } catch(\Exception $e) {
            $response->exception = $e->getMessage();
            $response->message = 'error';
            $response->code = 500;
        }
        return $response;
    }
}
