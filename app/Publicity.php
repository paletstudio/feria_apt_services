<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Image;
use Storage;

class Publicity extends Model
{
	use SoftDeletes;

	protected $table = 'publicidad';
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	protected $fillable = ['descripcion', 'fecha_inicio', 'fecha_fin', 'link', 'zona_id','activo'];
	protected $dates = ['deleted_at'];

	public function zone() {
		return $this->belongsTo('App\Zone', 'zona_id');
	}

	/**
	 * Get all the publicity.
	 *
	 * You could pass a query param to paginate. paginate = true
	 *
	 * @return Response
	 */
	public static function index()
	{
		$response = new Response();
		$paginate = request('paginate', false);
		$zone = request('zone', null);
		$query = Publicity::with(['zone']);

		if($zone) {
			$query->where('zona_id', $zone);
		}
		try {
			if ($paginate) {
				$publicity = $query->paginate(10);
			} else {
				$publicity = $query->get();
			}
			$response->data = $publicity;
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->message = 'error';
			$response->code = 500;
		}
		return $response;
	}

	/**
	 * Get one publicity by id.
	 *
	 * @param int $id Publicity id
	 * @return Response
	 */
	public static function show($id)
	{
		$response = new Response();
		try {
			$publicity = Publicity::findOrFail($id);
			$publicity->load('zone');
			$publicity->banner = url('storage/'.$publicity->banner);
			$response->data = $publicity;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->code = 500;
			$response->message = 'error';
		}
		return $response;
	}

	/**
	 * Create a new publicity
	 * @param  Publicity $publicity Publicity to create
	 * @return Response
	 */
	public static function store($request)
	{
		$response = new Response();
		try {
			$publicity = new Publicity();
			$image = Image::make($request['banner']);
			$image->stream();
			$filename = time().'.jpg';
			$filepath = "publicidad/{$filename}";
			Storage::put($filepath, $image);
			$request['banner'] = $filepath;
			$publicity->fill($request);
			$publicity->banner = $request['banner'];
			$publicity->save();
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Updates the given Publicity.
	 * @param  Publicity $request Publicity to edit
	 * @param  int $id      Publicity ID
	 * @return Response
	 */
	public static function edit($request, $id)
	{
		$response = new Response();
		try {
			$publicity = Publicity::findorFail($id);
			$publicity->fill($request);
			$publicity->save();
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Soft deletes an instance of publicity.
	 * @param  int $id Publicity to delete
	 * @return Response
	 */
	public static function remove($id)
	{
		$response = new Response();
		try {
			$publicity = Publicity::destroy($id);
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	public static function activePublicity()
	{
		$response = new Response;
		$zone = request('zone', null);
		try {
			$now = date("Y-m-d");
			$query = Publicity::where([
				['fecha_inicio', '<=', $now],
				['fecha_fin', '>=', $now],
				['activo', 1]
			]);
			if($zone) {
				$query->where('zona_id', $zone);
			}
			$publicity = $query->get();
			if (count($publicity) > 0) {
				foreach ($publicity as $p) {
					$p->banner = url('storage/'.$p->banner);
				}
			}
			$response->data = $publicity;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;

	}
}
