<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Tarifas extends Model
{
    use SoftDeletes;

	protected $table = 'tarifas';
	protected $fillable = ['nombre', 'tipo', 'ac', 'precio', 'activo', 'fecha_tarifa', 'fecha_fin'];
	protected $dates = ['deleted_at'];

    /**
	 * Get all the rates.
	 *
	 * You could pass a query param to paginate. paginate = true
	 *
	 * @return Response
	 */
	public static function index()
	{
		$response = new Response();
		$paginate = request('paginate', false);
		try {
			if ($paginate) {
				$tarifas = Tarifas::orderBy('id')->paginate(10);
			} else {
				$tarifas = Tarifas::orderBy('id')->get();
			}
			$response->data = $tarifas;
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->message = 'error';
			$response->code = 500;
		}
		return $response;
	}

	/**
	 * Get one rate by id.
	 *
	 * @param int $id Tarifas id
	 * @return Response
	 */
	public static function show($id)
	{
		$response = new Response();
		try {
			$tarifa =Tarifas::findOrFail($id);
			$response->data = $tarifa;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->code = 500;
			$response->message = 'error';
		}
		return $response;
	}

	/**
	 * Create a new rate
	 * @param  Tarifas $tarifa Rate to create
	 * @return Response
	 */
	public static function store($request)
	{
		$response = new Response();
		try {
			$tarifa = new Tarifas();
			$tarifa->fill($request);
			$tarifa->save();
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Updates the given Tarifas.
	 * @param  Tarifas $request Tarifas to edit
	 * @param  int $id      Tarifas ID
	 * @return Response
	 */
	public static function edit($request, $id)
	{
		$response = new Response();
		try {
			$tarifa = Tarifas::findorFail($id);
			$tarifa->fill($request);
			$tarifa->save();
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Soft deletes an instance of tarifa.
	 * @param  int $id Tarifas to delete
	 * @return Response
	 */
	public static function remove($id)
	{
		$response = new Response();
		try {
			$tarifa = Tarifas::destroy($id);
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Obtener las tarifas activas en fecha
	 * @return Response
	 */
	public static function activeRates()
	{
		$response = new Response;
		try {
			$now = date("Y-m-d");
			$rate = Tarifas::where([
				['fecha_tarifa', '<=', $now],
				['fecha_fin', '>=', $now],
				['activo', 1]
			])->get();

			$response->data = $rate;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;

	}
}
