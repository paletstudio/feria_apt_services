<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use JWTAuth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }

    public static function login($credentials)
    {
        $response = new Response();

        try {
            $user = self::where('email', $credentials['email'])->first();
            if($user && \Hash::check($credentials['password'], $user->password)){
                $customClaims['user'] = new \stdClass;
                $customClaims['user']->id = $user->id;
                $customClaims['user']->name = $user->name;
                $customClaims['user']->email = $user->email;

                $response->token = JWTAuth::fromUser($user, $customClaims);
                $response->code = 200;
                $response->msg = 'Login con éxito';
            } else {
                $response->code = 401;
                $response->msg = 'Credenciales invalidas';
            }
        } catch (\Exception $e){
            $response->exception = $e->getMessage();
        }

        return $response;
    }

    public static function create($usr)
    {
        $response = new Response();
        try {
            $user = new self();
            $user->fill($usr);
            $user->save();

            $response->code = 201;
            $response->msg = 'Usuario creado correctamente';
        } catch (\Exception $e) {
            $response->msg = 'Se produjo un error al crear el usuario.';
            $response->exception = $e->getMessage();
            $response->code = 500;
        }

        return $response;
    }

}
