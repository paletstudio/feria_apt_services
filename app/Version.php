<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    protected $table = 'version';
    protected $fillable = ['version', 'date'];

    public static function get()
    {
        $response = new Response;
        try {
            $version = Version::orderBy('id', 'desc')->limit(1)->get();
            $response->data = $version[0];
            $response->code = 200;
        } catch(\Exception $e) {
            $response->code = 500;
            $response->message = 'error';
            $response->exception = $e->getMessage();
        }
        return $response;
    }

    public static function store($data)
    {
        $response = new Response;
        try {
            $version = new Version;
            $version->fill($data);
            $version->save();
            $response->data = $version;
            $response->code = 200;
        } catch(\Exception $e) {
            $response->code = 500;
            $response->message = 'error';
            $response->exception = $e->getMessage();
        }
        return $response;
    }

    public static function edit($data)
    {
        $response = new Response;
        try {
            $version = Version::find(1);
            $version->fill($data);
            $version->save();
            $response->data = $version;
            $response->code = 200;
        } catch(\Exception $e) {
            $response->code = 500;
            $response->message = 'error';
            $response->exception = $e->getMessage();
        }
        return $response;
    }
}
