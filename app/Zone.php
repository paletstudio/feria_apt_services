<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zona';
	protected $hidden = ['created_at', 'updated_at'];
	protected $fillable = ['nombre', 'descripcion', 'ancho', 'alto', 'costo', 'activo'];

	public function publicity(){
		return $this->hasMany('App\Publicity', 'zona_id');
	}

	/**
	 * Get all the zones.
	 *
	 * @return Response
	 */
	public static function index()
	{
		$response = new Response();
		try {
			$zone = Zone::all();
			$response->data = $zone;
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->message = 'error';
			$response->code = 500;
		}
		return $response;
	}

	/**
	 * Get one zone by id.
	 *
	 * @param int $id Zone id
	 * @return Response
	 */
	public static function show($id)
	{
		$response = new Response();
		try {
			$zone = Zone::findOrFail($id);
			$response->data = $zone;
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->exception = $e->getMessage();
			$response->code = 500;
			$response->message = 'error';
		}
		return $response;
	}

	/**
	 * Create a new zone
	 * @param  Zone $zone Zone to create
	 * @return Response
	 */
	public static function store($request)
	{
		$response = new Response();
		try {
			$zone = new Zone();
			$zone->fill($request);
			$zone->save();
			$response->message = 'success';
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Updates the given Zone.
	 * @param  Zone $request Zone to edit
	 * @param  int $id      Zone ID
	 * @return Response
	 */
	public static function edit($request, $id)
	{
		$response = new Response();
		try {
			$zone = Zone::findorFail($id);
			$zone->fill($request);
			$zone->save();
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

	/**
	 * Deletes an instance of zone.
	 * @param  int $id Zone to delete
	 * @return Response
	 */
	public static function remove($id)
	{
		$response = new Response();
		try {
			$zone = Zone::destroy($id);
			$response->message = 'success';
		} catch (ModelNotFoundException $e) {
			$response->code = 404;
			$response->message = 'not found';
			$response->exception = $e->getMessage();
		} catch (\Exception $e) {
			$response->code = 500;
			$response->message = 'error';
			$response->exception = $e->getMessage();
		}
		return $response;
	}

}
