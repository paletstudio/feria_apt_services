<?php

return [
	'app_id' => env('ONE_SIGNAL_APP_ID'),
	'api_key' => env('ONE_SIGNAL_API_KEY')
];