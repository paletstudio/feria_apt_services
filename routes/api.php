<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');
Route::post('create', 'UserController@create');

Route::get('advice', 'AdviceController@index');
Route::get('rate', 'TarifasController@index');
Route::get('publicity', 'PublicityController@index');
Route::get('zone', 'ZoneController@index');
Route::get('rate/getactive', 'TarifasController@activeRates');
Route::get('publicity/getactive', 'PublicityController@activePublicity');
Route::get('zone/{id}', 'ZoneController@show');
Route::get('version', 'VersionController@get');

Route::group(['middleware' => 'jwt.auth'], function() {
	Route::group(['prefix' => 'advice'], function() {
		Route::get('{id}', 'AdviceController@show');
		Route::post('', 'AdviceController@store');
		Route::put('{id}', 'AdviceController@edit');
		Route::delete('{id}', 'AdviceController@remove');
	});
	
	Route::group(['prefix' => 'rate'], function() {
		Route::get('{id}', 'TarifasController@show');
		Route::post('', 'TarifasController@store');
		Route::put('{id}', 'TarifasController@edit');
		Route::delete('{id}', 'TarifasController@remove');
	});
	
	Route::group(['prefix' => 'publicity'], function() {
		Route::get('{id}', 'PublicityController@show');
		Route::post('', 'PublicityController@store');
		Route::put('{id}', 'PublicityController@edit');
		Route::delete('{id}', 'PublicityController@remove');
	});
	
	Route::group(['prefix' => 'zone'], function() {
		Route::post('', 'ZoneController@store');
		Route::put('{id}', 'ZoneController@edit');
		Route::delete('{id}', 'ZoneController@remove');
	});
	
	Route::group(['prefix' => 'version'], function() {
		Route::post('', 'VersionController@store');
		Route::put('', 'VersionController@edit');
	});

	Route::group(['prefix' => 'notification'], function() {
		Route::post('', 'NotificationController@sendNotification');
	});
});
